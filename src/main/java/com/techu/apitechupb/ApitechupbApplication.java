package com.techu.apitechupb;

import com.techu.apitechupb.models.ProductModel;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@SpringBootApplication

public class ApitechupbApplication {

	public static ArrayList<ProductModel> productModels;


	public static void main(String[] args) {
		SpringApplication.run(ApitechupbApplication.class, args);


		ApitechupbApplication.productModels = ApitechupbApplication.getTestData();

	}

	private static ArrayList<ProductModel> getTestData(){
		ArrayList<ProductModel> productModels = new ArrayList();

		productModels.add(new ProductModel("101","producto 101", (float)52.10) );

		return productModels;
	}

}

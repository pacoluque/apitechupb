package com.techu.apitechupb.controllers;

public class HttpError {

    String timestamp;
    String status;
    String error;
    String message;
    String path;
    Object recurso;

    public HttpError(String timestamp, String status, String error, String message, String path, Object recurso) {
        this.timestamp = timestamp;
        this.status = status;
        this.error = error;
        this.message = message;
        this.path = path;
        this.recurso = recurso;
    }

    public HttpError() {
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getStatus() {
        return status;
    }

    public String getError() {
        return error;
    }

    public String getMessage() {
        return message;
    }

    public String getPath() {
        return path;
    }

    public Object getRecurso() {
        return recurso;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setError(String error) {
        this.error = error;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setRecurso(Object recurso) {
        this.recurso = recurso;
    }
}

package com.techu.apitechupb.controllers;

import com.techu.apitechupb.ApitechupbApplication;
import com.techu.apitechupb.models.ProductModel;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;

@RestController
public class ProductController {


    static final String API_BASE_URL = "/apitechu/v1";

    @GetMapping(API_BASE_URL+ "/products")
    public ArrayList<ProductModel> getProducts(){

        System.out.println("getProducts");
        return ApitechupbApplication.productModels;
    }

    @GetMapping(API_BASE_URL+ "/products/{id}")
    public ProductModel getProductById(@PathVariable String id){

        System.out.println("getProductById");
        System.out.println("Id es:"+id);

        ProductModel result = new ProductModel();

        for (ProductModel product : ApitechupbApplication.productModels){
            if (product.getId().equals(id))
                result = product;
        }

        return result;
    }

    /**
     * Crea un producto en base a la información recibida en el body de la petición
     *
     * @param id
     * @param desc
     * @param price
     * @return
     */
    @PostMapping(API_BASE_URL+ "/products")
    public ProductModel createProduct(@RequestBody ProductModel newProduct){

        System.out.println("createProduct");
        System.out.println("Id es:"+newProduct.getId());

        ApitechupbApplication.productModels.add(newProduct);

        return newProduct;
    }

    /**
     *
     * @param product
     * @param id
     * @return
     */
    @PutMapping(API_BASE_URL+ "/products/{id}")
    public ProductModel updateProduct(@RequestBody ProductModel product, @PathVariable String id){

        System.out.println("updateProduct");
        System.out.println("Id es:"+product.getId());
        System.out.println("Precio es:"+product.getPrice());

        for (ProductModel productInList : ApitechupbApplication.productModels) {
            if (productInList.getId().equals(id)) {
                productInList.setId(product.getId());
                productInList.setPrice(product.getPrice());
                productInList.setDesc(product.getDesc());
            }
        }

        return product;
    }

    /**
     *
     * @param id
     * @return
     */
    @DeleteMapping(API_BASE_URL+ "/products/{id}")
    public ProductModel deleteProduct(@PathVariable String id) {

        System.out.println("deleteProduct");
        System.out.println("Id es:" + id);

        ProductModel result = new ProductModel();
        boolean foundProduct = false;

        for (ProductModel productInList : ApitechupbApplication.productModels) {
            if (productInList.getId().equals(id)) {
                System.out.println("Hemos encontrado el elemento a borrar");
                foundProduct = true;
                result = productInList;
            }
        }

        if (foundProduct){
            System.out.println(("Borrando Producto"));
            ApitechupbApplication.productModels.remove(result);
        }


        return result;
    }

    /**
     *
     * @param product
     * @param id
     * @return
     */
    @PatchMapping(API_BASE_URL+ "/products/{id}")
    @ResponseBody
    public HttpError patchProduct(@RequestBody ProductModel product, @PathVariable String id, HttpServletResponse response){

        System.out.println("patchProduct");
        System.out.println("Id a actualizar es:"+id);

        ProductModel foundProduct = null;
        HttpError salida = new HttpError();

        RuntimeException exception = new RuntimeException();

        // Localizamos el producto
        for (ProductModel productInList : ApitechupbApplication.productModels) {
            if (productInList.getId().equals(id)) {
                System.out.println("Hemos encontrado el elemento a modificar con PATCH");
                foundProduct = productInList;
            }
        }

        // Actualizamos el producto
        if (foundProduct != null){
            if (product.getDesc() != null)
                foundProduct.setDesc(product.getDesc());

            if (product.getPrice() > 0)
                foundProduct.setPrice(product.getPrice());

            response.setStatus(HttpServletResponse.SC_OK);
            salida.setError("OK");
            salida.setStatus("200");
            salida.setMessage("Modificación realizada con éxito");
            salida.setPath("/products/"+id);
            salida.setTimestamp("hhmmss");
            salida.setRecurso(foundProduct);
        }
        else {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            salida.setError("KO");
            salida.setStatus("404");
            salida.setMessage("Recurso no encontrado");
            salida.setPath("/products/"+id);
            salida.setTimestamp("hhmmss");
            salida.setRecurso(null);

        }

        return salida;
    }


}
